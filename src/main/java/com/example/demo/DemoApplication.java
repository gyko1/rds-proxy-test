package com.example.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootApplication
@RestController
public class DemoApplication {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DatabaseConfig dataSource;
	
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
	

	@GetMapping("/")
	String home() throws SQLException {
		return "Server is Healthy!";
	}
	
	@GetMapping("/rds-test/{cnt}")
	String rdsTest(@PathVariable int cnt ) throws SQLException, JsonProcessingException {
		
		long beforeTime = System.currentTimeMillis();
		logger.error("rds-dbcp-test start : test count{}", cnt);			
		
		
		String sql = "SELECT now()";
		List<Integer> selectList = new ArrayList<Integer>();
		
		for(int i = 0 ; i < cnt ; i++) {
			selectList.add(i);
		}
		
		
		List<Map<String, String>> resultList =  selectList.stream()
				.map(seq -> {
					return CompletableFuture.supplyAsync(() -> {
						
						String now = "";
						Map<String, String> resultMap = new HashMap<String, String>();
						
						try(Connection connection = dataSource.dataSource().getConnection()){
//							logger.error("connection : {}", connection.toString());
//							String URL = connection.getMetaData().getURL();
//							logger.debug(URL);
//							String User = connection.getMetaData().getUserName();
//							logger.debug(User);
							Statement statement = connection.createStatement();
							ResultSet  rs = statement.executeQuery(sql);
							
							while(rs.next()) {
					            now = rs.getString(1);
					        }
//							logger.debug("seq : {}, now : {}", seq, now);
							resultMap.put(String.valueOf(seq), now);
						} catch (SQLException e) {
							logger.error(e.getMessage());
						}
						return resultMap;
					});
				}).map(CompletableFuture::join).parallel().collect(Collectors.toList());
		
		
		long afterTime = System.currentTimeMillis(); // 코드 실행 후에 시간 받아오기
		long secDiffTime = (afterTime - beforeTime); //두 시간에 차 계산
		
//		ObjectMapper om = new ObjectMapper();
//		om.writerWithDefaultPrettyPrinter();
//		om.enable(SerializationFeature.INDENT_OUTPUT);
//		String resultJson = om.writeValueAsString(resultList);;
//		logger.error(resultJson);
		
		logger.error("시간차이(ms) : "+secDiffTime);
		
		return "작업완료 : " + secDiffTime +"ms";
	}
	
	
	@GetMapping("/rds-test-slow-query/{cnt}")
	String rdsTestSlowQuery(@PathVariable int cnt ) throws SQLException, JsonProcessingException {
		
		long beforeTime = System.currentTimeMillis();
		logger.error("rds-dbcp-test start : test count{}", cnt);			
		
//		String sql = "select SUM(0) from ("
//				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
//				+ " union all "
//				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
//				+ " union all "
//				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
//				+ ") A";
		
		String sql = "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code";		
		List<Integer> selectList = new ArrayList<Integer>();
		
		for(int i = 0 ; i < cnt ; i++) {
			selectList.add(i);
		}
		
		
		List<Map<String, String>> resultList =  selectList.stream()
				.map(seq -> {
					return CompletableFuture.supplyAsync(() -> {
						
						String now = "";
						Map<String, String> resultMap = new HashMap<String, String>();
						
						try(Connection connection = dataSource.dataSource().getConnection()){
							logger.error("connection : {}", connection.toString());
//							String URL = connection.getMetaData().getURL();
//							logger.debug(URL);
//							String User = connection.getMetaData().getUserName();
//							logger.debug(User);
							Statement statement = connection.createStatement();
							ResultSet  rs = statement.executeQuery(sql);
							
							while(rs.next()) {
								now = rs.getString(1);
							}
//							logger.debug("seq : {}, now : {}", seq, now);
							resultMap.put(String.valueOf(seq), now);
						} catch (SQLException e) {
							logger.error(e.getMessage());
						}
						return resultMap;
					});
				}).map(CompletableFuture::join).parallel().collect(Collectors.toList());
		
		
		long afterTime = System.currentTimeMillis(); // 코드 실행 후에 시간 받아오기
		long secDiffTime = (afterTime - beforeTime); //두 시간에 차 계산
		
//		ObjectMapper om = new ObjectMapper();
//		om.writerWithDefaultPrettyPrinter();
//		om.enable(SerializationFeature.INDENT_OUTPUT);
//		String resultJson = om.writeValueAsString(resultList);;
//		logger.error(resultJson);
		
		logger.error("시간차이(ms) : "+secDiffTime);
		
		return "작업완료 : " + secDiffTime +"ms";
	}
	@GetMapping("/rds-ro-test-slow-query/{cnt}")
	String rdsNonDbcpTestSlowQuery(@PathVariable int cnt ) throws SQLException, JsonProcessingException {
		
		
		long beforeTime = System.currentTimeMillis();
		logger.error("rds-dbcp-test start : test count{}", cnt);			
		
		String sql = "select SUM(0) from ("
				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
				+ " union all "
				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
				+ " union all "
				+ "select COUNT(1) from s3_mig a full outer join s3_mig b on a.code =b.code"
				+ ") A";
		
		List<Integer> selectList = new ArrayList<Integer>();
		
		for(int i = 0 ; i < cnt ; i++) {
			selectList.add(i);
		}
		
		List<Map<String, String>> resultList =  selectList.stream()
				.map(seq -> {
					return CompletableFuture.supplyAsync(() -> {
						
						String now = "";
						Map<String, String> resultMap = new HashMap<String, String>();
						
						try {
							Class.forName("org.postgresql.Driver");
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Connection con = null;
						PreparedStatement pstmt = null;
						try {
							con = DriverManager.getConnection("jdbc:postgresql://thm-prd-order-cluster.cluster-c4waczuyyyc6.ap-northeast-2.rds.amazonaws.com:5432/thehandsome","op_adm","opadm12#$");
							con.setAutoCommit(true);
							pstmt = con.prepareStatement(sql);
							
							if(pstmt != null) pstmt.close();
							if(con != null) con.close();
							
						} catch (Exception e) {
							
						} finally {
							
						}
						return resultMap;
					});
				}).map(CompletableFuture::join).parallel().collect(Collectors.toList());
		
		
		long afterTime = System.currentTimeMillis(); // 코드 실행 후에 시간 받아오기
		long secDiffTime = (afterTime - beforeTime); //두 시간에 차 계산
		
//		ObjectMapper om = new ObjectMapper();
//		om.writerWithDefaultPrettyPrinter();
//		om.enable(SerializationFeature.INDENT_OUTPUT);
//		String resultJson = om.writeValueAsString(resultList);;
//		logger.error(resultJson);
		
		logger.error("시간차이(ms) : "+secDiffTime);
		
		return "작업완료 : " + secDiffTime +"ms";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
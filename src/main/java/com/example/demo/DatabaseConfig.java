package com.example.demo;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfig {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
//	private static final char[] ID_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private static final long CONNECTION_TIMEOUT = SECONDS.toMillis(30);
	private static final long VALIDATION_TIMEOUT = SECONDS.toMillis(5);
	private static final long IDLE_TIMEOUT = MINUTES.toMillis(10);
	private static final long MAX_LIFETIME = MINUTES.toMillis(2);
//	private static final int DEFAULT_POOL_SIZE = 10;
	private static final int MIN_IDLE_POOL_SIZE = 10;
	private static final int MAX_POOL_SIZE = 100;
    
	@Bean
    @ConfigurationProperties(prefix = "spring.datasource.hikari")
    public HikariConfig hikariConfig() {
    	HikariConfig hikariConfig = new HikariConfig();
    	
    	hikariConfig.setMinimumIdle(MIN_IDLE_POOL_SIZE);
        hikariConfig.setMaximumPoolSize(MAX_POOL_SIZE);
//        hikariConfig.setMaxLifetime(MAX_LIFETIME);
        hikariConfig.setConnectionTimeout(CONNECTION_TIMEOUT);
        hikariConfig.setValidationTimeout(VALIDATION_TIMEOUT);
        hikariConfig.setIdleTimeout(IDLE_TIMEOUT);
//        hikariConfig.setInitializationFailTimeout(1);
        hikariConfig.setAutoCommit(true);
        
        return hikariConfig;
//        return new HikariConfig();
    }

    @Bean
    public DataSource dataSource() {
    	DataSource dataSource = new HikariDataSource(hikariConfig());
    	logger.error("dataSource.toString() : {}", dataSource.toString());
		return dataSource;
    }
}